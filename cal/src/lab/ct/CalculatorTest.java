package lab.ct;

public class CalculatorTest {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        int add = calculator.add(10,5);
        int sub = calculator.sub(10,5);
        int mul = calculator.mul(10,5);
        int div = calculator.div(10,5);
        int mod = calculator.mod(10,5);
        long pow = calculator.pow(10,5);

        System.out.println("add : " + add);
        System.out.println("sub : " + sub);
        System.out.println("mul : " + mul);
        System.out.println("div : " + div);
        System.out.println("mod : " + mod);
        System.out.println("pow : " + pow);
    }
}
