package lab.ct;
import java.lang.Math;

public class Calculator {
    public int add(int x, int y){
        return x+y;
    }
    public int sub(int x, int y){
        return x-y;
    }
    public int mul(int x, int y){
        return x*y;
    }
    public int div(int x, int y){
        return x/y;
    }
    public int mod(int x, int y){
        return x%y;
    }
    public long pow(int x, int y){
        return (long) Math.pow(x,y);
    }
}
