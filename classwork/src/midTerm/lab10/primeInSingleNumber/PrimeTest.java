package midTerm.lab10.primeInSingleNumber;

import java.util.Scanner;

public class PrimeTest {
    public static void main(String[] args) {
        int testNumber;
        int i; // to count loop
        int factors; // to count factor

        factors=0; // initialize factor value;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a number to check whether it is prime or not : ");

        testNumber = scanner.nextInt();

        for(i=1; i<=testNumber; i++){
            if(testNumber%i == 0){
                factors++; // increase factor value if it is divisible

                if(factors > 2){
                    // if the factor value is 3 or more then it stops the loop
                    break;
                }
                else{
                    // if the factor value is less than 2 or 2 then the loop will be continue
                    continue;
                }
            }
        }

        if(factors == 2){
            System.out.println("the number is prime.");
        }

        else {
            System.out.println("the number is not prime.");
        }
    }
}
