package midTerm.lab5;

import java.util.Scanner;

public class CircleTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Circle circle = new Circle();

        System.out.println("Enter the radius : ");
        circle.radius = scanner.nextDouble();

        System.out.println("Area is = " + circle.calArea());
        System.out.println("Perimeter is = " + circle.calPerimeter());
    }
}

class Circle{
    double radius;

    double calArea(){
        return 3.1416*radius*radius;
    }

    double calPerimeter(){
        return 2*3.1416*radius;
    }
}
