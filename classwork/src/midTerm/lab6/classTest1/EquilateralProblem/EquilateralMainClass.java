package midTerm.lab6.classTest1.EquilateralProblem;

public class EquilateralMainClass {
    public static void main(String[] args) {
        Equilateral equilateral = new Equilateral();

        equilateral.side = 5;

        System.out.println("The area of the equilateral is : " + equilateral.calArea());
        System.out.println("The perimeter of the equilateral is : " + equilateral.calPerimeter());
    }
}
