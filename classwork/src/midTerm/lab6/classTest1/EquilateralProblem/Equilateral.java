package midTerm.lab6.classTest1.EquilateralProblem;

public class Equilateral {
    double side;

    public double calArea(){
        double height = Math.sqrt((side*side) - (side/2)*(side/2));
        return (height*side)/2;
    }

    public double calPerimeter(){
        return 3*side;
    }
}
