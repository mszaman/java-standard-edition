package midTerm.lab6.classTest1.RightAngledProblem;

public class RightAngledMainClass {
    public static void main(String[] args) {
        RightAngled rightAngled = new  RightAngled();

        rightAngled.height = 5;
        rightAngled.base = 6;

        System.out.println("The area of the right angled triangle is : " + rightAngled.calArea());
        System.out.println("The perimeter of the right angled triangle is : " + rightAngled.calPerimeter());
    }
}
