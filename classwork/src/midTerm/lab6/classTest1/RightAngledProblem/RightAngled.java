package midTerm.lab6.classTest1.RightAngledProblem;

public class RightAngled {
    double height;
    double base;

    public double calArea(){
        return height*base;
    }

    public double calPerimeter(){
        double hypotenuse = Math.sqrt(height*height + base*base);
        return hypotenuse+height+base;
    }
}
