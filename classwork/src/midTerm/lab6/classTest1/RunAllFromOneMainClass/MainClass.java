package midTerm.lab6.classTest1.RunAllFromOneMainClass;

public class MainClass {
    public static void main(String[] args) {
        RightAngled rightAngled = new RightAngled();
        Eqiulateral eqiulateral = new Eqiulateral();
        Isosceles isosceles = new Isosceles();

        rightAngled.height = 6;
        rightAngled.base = 5;

        eqiulateral.side = 6;

        isosceles.base = 5;
        isosceles.side = 6;

        System.out.println("The area of the right angle triangle is : " + rightAngled.calArea());
        System.out.println("The perimeter of the right angle triangle is : " + rightAngled.calPerimeter());
        System.out.println("The area of the equilateral is : " + eqiulateral.calArea());
        System.out.println("The perimeter of the equilateral is : " + eqiulateral.calPerimeter());
        System.out.println("The area of the isosceles is : " + isosceles.calArea());
        System.out.println("The perimeter of the isosceles is : " + isosceles.calPerimeter());
    }
}
