package midTerm.lab6.classTest1.IsoscelesProblem;

public class IsoscelesMainClass {
    public static void main(String[] args) {
        Isosceles isosceles = new Isosceles();

        isosceles.base = 5;
        isosceles.side = 7;

        System.out.println("The area of Isoscelis is : " + isosceles.calArea());
        System.out.println("The perimeter of Isoscelis is : " + isosceles.calPerimeter());
    }
}
