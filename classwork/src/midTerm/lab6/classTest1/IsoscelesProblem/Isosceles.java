package midTerm.lab6.classTest1.IsoscelesProblem;

public class Isosceles {
    double base;
    double side;

    public double calArea(){
        double heiht = Math.sqrt((side*side) - (base/2)*(base/2));
        return (heiht*base)/2;
    }

    public double calPerimeter(){
        return (2*side)+base;
    }
}
