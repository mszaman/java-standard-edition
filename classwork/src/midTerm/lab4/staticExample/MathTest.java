package midTerm.lab4.staticExample;

import java.util.Scanner;

public class MathTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter first number : ");
        int firstNumber = scanner.nextInt();
        System.out.println("Enter second number : ");
        int secondNumber = scanner.nextInt();

        /*
        do't need to create any object under Arithmetic class like --->
        Arithmetic obj = new Arithmetic();
         */

        System.out.println("Summation = " + Arithmetic.add(firstNumber, secondNumber));
        System.out.println("Subtraction = " + Arithmetic.sub(firstNumber, secondNumber));
        System.out.println("Multiplication = " + Arithmetic.mul(firstNumber, secondNumber));
        System.out.println("Division = " + Arithmetic.div(firstNumber, secondNumber));
        System.out.println("Remainder = " + Arithmetic.mod(firstNumber, secondNumber));
    }
}
