package midTerm.lab4.staticExample;

public class Arithmetic {
    static int add(int a, int b) {
        return a+b;
    }

    static int sub(int a, int b){
        return a-b;
    }

    static int mul(int a, int b){
        return a*b;
    }

    static double div(int a, int b){
        return a/b;
    }

    static int mod(int a, int b){
        return a%b;
    }
}
