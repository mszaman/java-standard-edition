package midTerm.lab3;

import java.util.Scanner;

public class InputFromKeyboard {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your name : ");
        String name = scanner.nextLine();
        System.out.println("My name is " + name);

        System.out.println("Enter your id : ");
        int id = scanner.nextInt();
        System.out.println("My ID is " + id);

        System.out.println("Enter your department : ");
        String dept = scanner.next();
        System.out.println("My department is " + dept);

        System.out.println("Enter your semester : ");
        String semester = scanner.next();
        System.out.println("My semester is " + semester);
    }
}
