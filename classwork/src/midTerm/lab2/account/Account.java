package midTerm.lab2.account;

public class Account {
    String accName;
    int accNo;
    double balance;

    void diposit(double b){
        balance = balance + b;
        System.out.println("Taka " + b + " is diposited successfully.");
        System.out.println("Your current balance is " + balance + "taka.");
    }

    void withdraw(double b){
        if(balance < b)
            System.out.println("Insufficient balance.");
        else{
            balance -= b;
            System.out.println(b + " taka is withdrawn from your account.");
            System.out.println("Your current balance is " + balance + " taka.");
        }
    }
}
