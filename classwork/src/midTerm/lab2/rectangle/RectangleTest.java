package midTerm.lab2.rectangle;

public class RectangleTest {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();

        rectangle.length = 8;
        rectangle.width = 6;

        System.out.println("Area is = " + rectangle.calArea());
        System.out.println("Perimeter is = " + rectangle.calPerimeter());
        System.out.println("Diagonal is = " + rectangle.calDiagonal());
    }
}

class Rectangle {
    double length;
    double width;

    double calArea(){
        return length*width;
    }

    double calPerimeter(){
        return 2*(length+width);
    }

    double calDiagonal(){
        return Math.sqrt(width*width+length*length);
    }
}
